#!/bin/bash
# Generates rss file that can be used to import new videos into amara
# Amara just add the video to the team but not to a project. You need
# to rename the projectless projects using amara.py
#
# The funtionality of this script is supposed to be superseeded by amara.py
# using the api to upload, but im not sure it works fine yet

default='2016/mini-debconf-vienna/webm'
url='http://meetings-archive.debian.net/pub/debian-meetings/'

if [ $# -ne 1 ]; then
	echo "Usage: $0 <year/event-path-without-trailing-slash/video-folder>"
	echo "	example: $0 '$default'"
	echo
	echo "	Url base: $url"
	exit
fi

final_url=$url$1/
file="${1//\//_}".xml

echo "Does $final_url look fine? [Y]/Ctrl-C"
read

echo "Saving to $file"

cat << EOF > $file
<?xml version="1.0" encoding="utf-8"?>  <rss version="2.0">  <channel>
EOF

curl -s $final_url'?C=M;O=A' |grep VID |cut -d\" -f6|

while read video; do

cat << EOF >> $file
<item>
<title> $video </title>
<link>
EOF

	echo -n "$final_url$video"

cat << EOF >> $file
</link>
</item>
EOF

done >> $file

cat << EOF >> $file
</channel>
EOF
