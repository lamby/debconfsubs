1
00:00:04,491 --> 00:00:07,300
Hello, Thank you for coming

2
00:00:07,480 --> 00:00:13,785
We're gonna give a talk about and 
gonna give a technical overview of Tails.

3
00:00:15,720 --> 00:00:20,480
That's kurono, intrigeri 
and I am BitingBird.

4
00:00:30,500 --> 00:00:35,260
We are all Tails contributors
in different fields.

5
00:00:37,730 --> 00:00:41,630
I don't do technical things,

6
00:00:41,760 --> 00:00:47,740
intrigeri is one of the 
oldest tails contributors

7
00:00:47,740 --> 00:00:54,500
and kurono contributes 
since two years now.

8
00:00:56,400 --> 00:01:01,740
Tails is the acronym of
The Amnesic Incognito Live System

9
00:01:02,070 --> 00:01:06,940
And here is the nice url, 
where you can have all the information.

10
00:01:08,621 --> 00:01:10,531
It's a live operating system.

11
00:01:10,730 --> 00:01:15,460
It works on almost any computer -
except ARM

12
00:01:16,480 --> 00:01:20,830
And it boots from a dvd or a usb stick

13
00:01:20,830 --> 00:01:25,280
and theoretically from sdcard too, 
but it doesn't work very well.

14
00:01:28,970 --> 00:01:34,590
The focus of our distribution 
is privacy and anonymity.

15
00:01:35,471 --> 00:01:40,491
It allows the user 
to use the internet anonymously.

16
00:01:43,040 --> 00:01:47,480
And also, when there is censorship,
to circumvent it.

17
00:01:48,931 --> 00:01:52,111
All the connections to 
the internet go with tor,

18
00:01:52,461 --> 00:01:56,041
which is an anonymization network.

19
00:01:57,720 --> 00:02:02,200
That's the first big feature of tails.

20
00:02:02,200 --> 00:02:03,691
And the second one is

21
00:02:03,691 --> 00:02:06,200
that there is no trace 
on the computer you are using

22
00:02:06,200 --> 00:02:13,990
so after you used it nobody can see
that you've used the computer.

23
00:02:15,930 --> 00:02:20,601
If somebody would grab your computer 
and search files

24
00:02:20,601 --> 00:02:23,551
they would not know, 
what you have done.

25
00:02:25,250 --> 00:02:29,100
Unless you ask for it explicitly &lt;????&gt;

26
00:02:29,721 --> 00:02:36,721
We have also a lot of data producing tools

27
00:02:38,000 --> 00:02:47,761
because some users use it to write books,
articles, video and such things.

28
00:02:48,651 --> 00:02:54,410
They want to be able to create such documents without being traced.

29
00:02:58,460 --> 00:03:01,640
Does it work ?

30
00:03:01,640 --> 00:03:04,890
We have a very good report,

31
00:03:04,891 --> 00:03:06,901
not from our users,

32
00:03:06,901 --> 00:03:11,840
actually from the people 
we are supposed to protect them against.

33
00:03:11,840 --> 00:03:16,260
The NSA says, that it's a pain in the ass.

34
00:03:16,483 --> 00:03:22,751
When the NSA says 
you're making their life harder

35
00:03:22,751 --> 00:03:26,230
somehow you're doing something right.

36
00:03:26,230 --> 00:03:31,490
[klapping, laughing]

37
00:03:31,500 --> 00:03:37,490
I guess you can imagine who's 
the famous tails user

38
00:03:37,490 --> 00:03:41,600
who gave us access to the documents where
they say that

39
00:03:42,790 --> 00:03:48,530
There is also Bruce Schneier
who says he uses Tails

40
00:03:49,231 --> 00:03:53,681
so, not bad.

41
00:03:54,601 --> 00:03:57,351
So, what are our goals?

42
00:03:57,480 --> 00:04:01,692
We took a stance in the beginning of Tails

43
00:04:01,692 --> 00:04:04,513
that it was not really common back then

44
00:04:04,513 --> 00:04:08,110
to have usability as a security feature

45
00:04:08,110 --> 00:04:13,540
because "ubergeeks" where already able
to have secure communication.

46
00:04:15,221 --> 00:04:18,530
The thing is privacy 
is not an individual matter.

47
00:04:18,530 --> 00:04:19,941
It's a collective matter.

48
00:04:19,941 --> 00:04:22,801
Everybody needs to have privacy

49
00:04:22,801 --> 00:04:32,501
and new users and non geek users 
had no way to get access to this.

50
00:04:32,502 --> 00:04:36,530
The tools existed but they had 
no user interface

51
00:04:36,530 --> 00:04:39,730
or they where really hard to configure.

52
00:04:39,730 --> 00:04:45,741
So, we designed a system that gives 
a quite good level of security

53
00:04:45,741 --> 00:04:49,031
with a quite good level of usability.

54
00:04:49,480 --> 00:04:54,760
Lots of the time people ask us, why we 
don't include more security features.

55
00:04:54,760 --> 00:04:58,610
We have to make a balance between security and usability.

56
00:04:58,611 --> 00:05:02,230
Because if it's really secure
but nobody can use it

57
00:05:02,230 --> 00:05:05,931
then it doesn't bring anything.

58
00:05:05,931 --> 00:05:10,171
It makes security accessible
for most people.

59
00:05:12,464 --> 00:05:15,912
Another important point in our project

60
00:05:15,912 --> 00:05:20,242
is to have a very small delta 
to our upstream.

61
00:05:21,660 --> 00:05:28,510
Our main upstream is Debian and we try 
to not diverge too much from it.

62
00:05:30,500 --> 00:05:34,100
Because the more you do things differently

63
00:05:34,100 --> 00:05:37,501
the more work you have to maintain.

64
00:05:37,501 --> 00:05:41,363
The work is not the work of 
implementing something once

65
00:05:41,363 --> 00:05:44,663
it's the work of 
maintaining on the long term.

66
00:05:45,262 --> 00:05:49,772
There where a lot of other 
security distributions

67
00:05:49,772 --> 00:05:51,903
and there are still a few others

68
00:05:51,903 --> 00:05:55,523
But most of them 
have a very short lifespan

69
00:05:57,814 --> 00:06:02,020
because of maintenance.

70
00:06:02,020 --> 00:06:04,756
It's a distribution and

71
00:06:04,756 --> 00:06:07,632
we're a very tiny team compared to Debian

72
00:06:07,632 --> 00:06:10,283
but we're a team.

73
00:06:10,283 --> 00:06:15,290
Lots of other privacy distributions
where either one person

74
00:06:15,290 --> 00:06:22,290
or very tiny teams and they didn't make 
outrage to be joined by other people

75
00:06:24,440 --> 00:06:32,752
Most other privacy distributions didn't 
take into account the maintenance work

76
00:06:32,752 --> 00:06:35,501
and the user support because

77
00:06:35,501 --> 00:06:37,761
even if we try to make it usable

78
00:06:37,761 --> 00:06:43,000
it's still a lot of work to 
teach the users how to use it

79
00:06:43,000 --> 00:06:46,806
and to document how to use it.

80
00:06:46,806 --> 00:06:49,270
Also if you want to start such a project

81
00:06:49,270 --> 00:06:52,741
you need to have a long term commitment

82
00:06:52,741 --> 00:06:57,301
and to remember to avoid the symptom of
"not invented here".

83
00:06:57,691 --> 00:07:03,392
It's quite common to try to do something 
that does exactly what you want

84
00:07:03,392 --> 00:07:08,031
but sometimes it's best 
to find an existing software

85
00:07:08,031 --> 00:07:11,781
that does something close enough

86
00:07:11,781 --> 00:07:18,250
to make the new features you want in it 
or use it as it is.

87
00:07:20,970 --> 00:07:23,770
We are trying to do most of our work,

88
00:07:23,770 --> 00:07:26,790
at least a good part of our work upstream

89
00:07:26,790 --> 00:07:30,014
so we did AppArmor

90
00:07:30,014 --> 00:07:32,930
in Debian specifically there is 
an AppArmor team,

91
00:07:32,930 --> 00:07:37,360
an anonymity tools team and an OTR team

92
00:07:37,361 --> 00:07:42,001
who work on things that we use in Tails

93
00:07:42,501 --> 00:07:49,271
libvirt, Seahorse, Tor and Puppet 
are other projects we contributed to

94
00:07:49,271 --> 00:07:54,631
instead of implementing ourselves 
what we need in Tails

95
00:07:54,631 --> 00:08:01,510
we did it upstream 
and it took longer to fall down to us

96
00:08:01,510 --> 00:08:04,470
but it's maintainable.

97
00:08:04,470 --> 00:08:07,552
When we finally have the new features

98
00:08:07,552 --> 00:08:12,102
we have no work of keeping them.

99
00:08:13,490 --> 00:08:17,590
As a result we have 
really little Tails specific code

100
00:08:17,590 --> 00:08:20,640
we mostly do glue work between the code

101
00:08:20,640 --> 00:08:23,774
we take from our upstreams

102
00:08:23,774 --> 00:08:25,914
and we do a lot of social work

103
00:08:25,920 --> 00:08:29,250
we talk to upstream, we spread the word

104
00:08:29,250 --> 00:08:34,660
we say "Oh that would be great if somebody
where to work on that"

105
00:08:35,970 --> 00:08:40,501
And we find the people that 
have the right skills

106
00:08:40,501 --> 00:08:44,861
to do the work that should be done
when it's not in Tails

107
00:08:46,010 --> 00:08:51,450
We have a very slow rythm 
because we work in Debian

108
00:08:51,450 --> 00:08:55,230
so we have to wait until the next Debian version is released

109
00:08:55,230 --> 00:08:59,791
To see the work we have done in Tails 
as AppArmor

110
00:08:59,791 --> 00:09:02,826
I mentioned earlier, we did it in Debian

111
00:09:02,826 --> 00:09:09,276
so for two years there was work going on
in Debian that was not visible in Tails

112
00:09:09,283 --> 00:09:14,543
but we finally have it

113
00:09:16,480 --> 00:09:21,100
Tails is still alive, 
because it's maintainable

114
00:09:22,720 --> 00:09:24,404
Implementation details -

115
00:09:24,404 --> 00:09:30,704
That's where I give the micro.
[gives micro to kurono]
