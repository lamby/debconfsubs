1
99:59:59,999 --> 99:59:59,999
[Talk meister] Grab a seat please, there's
lots of seats around here, or there.

2
99:59:59,999 --> 99:59:59,999
It's my pleasure to introduce our current
and new DPL,

3
99:59:59,999 --> 99:59:59,999
Neil McGovern for his annual bits from
the DPL

4
99:59:59,999 --> 99:59:59,999
[Applause]

5
99:59:59,999 --> 99:59:59,999
[Neil] Thanks very much, hi everyone.

6
99:59:59,999 --> 99:59:59,999
I'm aware that this talk is due to cut in
to lunch so I'll try and keep it slightly

7
99:59:59,999 --> 99:59:59,999
... in to dinner, so I'll keep it slightly
more refined [laughter]

8
99:59:59,999 --> 99:59:59,999
so people can get their food early.

9
99:59:59,999 --> 99:59:59,999
Hopefully everyone's ok with that anyway.

10
99:59:59,999 --> 99:59:59,999
I'm also kind of glad that it's in this
slot when it was originally proposed

11
99:59:59,999 --> 99:59:59,999
and I got my initial time slot it was
going to be a 9am slot just after the

12
99:59:59,999 --> 99:59:59,999
cheese and wine party so I [laughter]

13
99:59:59,999 --> 99:59:59,999
probably wouldn't expect quite so many
people as we have here today.

14
99:59:59,999 --> 99:59:59,999
So, welcome everyone, this final day of
the open weekend. Anyone who isn't a

15
99:59:59,999 --> 99:59:59,999
regular person, hi and welcome to Debian!

16
99:59:59,999 --> 99:59:59,999
Welcome to the huge Debian family which
is certainly growing all the time and

17
99:59:59,999 --> 99:59:59,999
is something that's really good to see.

18
99:59:59,999 --> 99:59:59,999
It's quite fantastic that there's just so
many people involved, especially this year

19
99:59:59,999 --> 99:59:59,999
When I last checked the stats I think we
had 383 people who had so far checked in

20
99:59:59,999 --> 99:59:59,999
slightly less than for DebConf 7, but I'm
certain... [laughter]

21
99:59:59,999 --> 99:59:59,999
I'm certain over the next few days that'll
go up and definitely surpass us.

22
99:59:59,999 --> 99:59:59,999
I just want to remind everyone what the
size of the project we are.

23
99:59:59,999 --> 99:59:59,999
It's such a huge effort, one of the
biggest open source and free software

24
99:59:59,999 --> 99:59:59,999
projects in the world.

25
99:59:59,999 --> 99:59:59,999
Combining around 1000 developers, a few
thousand maintainers and contributors as

26
99:59:59,999 --> 99:59:59,999
well, and also our users.

27
99:59:59,999 --> 99:59:59,999
It's really a huge effort that we manage
to still be here after 22 years and still

28
99:59:59,999 --> 99:59:59,999
be going strong.

29
99:59:59,999 --> 99:59:59,999
Hopefully a DebConf in future will look
a little bit like that

30
99:59:59,999 --> 99:59:59,999
I think the orga team might be slightly
more stressed if we end up with numbers

31
99:59:59,999 --> 99:59:59,999
but what a fantastic thing.

32
99:59:59,999 --> 99:59:59,999
Also over the last few years we've had 42
new project members have joined us.

33
99:59:59,999 --> 99:59:59,999
This is over the last year, we've had all
these people.

34
99:59:59,999 --> 99:59:59,999
Huge welcome to everyone there. [Applause]

35
99:59:59,999 --> 99:59:59,999
Especially one or two on there which I
decided to get back into doing application

36
99:59:59,999 --> 99:59:59,999
management, especially for our non-
uploading developers as well, which has

37
99:59:59,999 --> 99:59:59,999
been a huge boost to really grow where
we're going.

38
99:59:59,999 --> 99:59:59,999
Also welcome back. I've noticed a few
people this year who I haven't seen in a

39
99:59:59,999 --> 99:59:59,999
few years, so stand up if you've been away
for a few years and you're returning

40
99:59:59,999 --> 99:59:59,999
I know there's certainly some people,
excellent,

41
99:59:59,999 --> 99:59:59,999
Tolimar? [laughter] There he is!
[Applause]

42
99:59:59,999 --> 99:59:59,999
Kris Rose? Is he here? [points] Excellent.

43
99:59:59,999 --> 99:59:59,999
Was active before I even joined Debian,
and essentially has come back again, so

44
99:59:59,999 --> 99:59:59,999
it's great to see everyone back and thanks
very much for remembering us.

45
99:59:59,999 --> 99:59:59,999
There's been a few new people as well.

46
99:59:59,999 --> 99:59:59,999
Very, very new people who I haven't kinda
seen around before in previous DebConfs

47
99:59:59,999 --> 99:59:59,999
Where did all these children come from?
I think there must have been something...

48
99:59:59,999 --> 99:59:59,999
[laughter]

49
99:59:59,999 --> 99:59:59,999
I think the Switzerland DebConf, there
might have been something in the water,

50
99:59:59,999 --> 99:59:59,999
because it's suddenly been a huge... it's
absolutely fantastic to see not only

51
99:59:59,999 --> 99:59:59,999
our usual developers around, but their
families as well, and new people being

52
99:59:59,999 --> 99:59:59,999
introduced to Debian and to technology.

53
99:59:59,999 --> 99:59:59,999
The TecKids workshops I think are
absolutely fantastic initiative I think

54
99:59:59,999 --> 99:59:59,999
that really helps broaden Debian and what
we do.

55
99:59:59,999 --> 99:59:59,999
So, when I first became DPL, I always knew
Debian was a big thing in free software,

56
99:59:59,999 --> 99:59:59,999
in the communities and things we do.

57
99:59:59,999 --> 99:59:59,999
I went along to my running club

58
99:59:59,999 --> 99:59:59,999
and we went for a run,

59
99:59:59,999 --> 99:59:59,999
went to the pub afterwards as you do in
the UK,

60
99:59:59,999 --> 99:59:59,999
you can't have sport without excessive
alcohol consumption afterwards so it seems

61
99:59:59,999 --> 99:59:59,999
and about 7 or 8 people all came up to me

62
99:59:59,999 --> 99:59:59,999
and said "Hey, congratulations on being
DPL, I'm going to buy you a pint!", which

63
99:59:59,999 --> 99:59:59,999
[laughter]

64
99:59:59,999 --> 99:59:59,999
which is great

65
99:59:59,999 --> 99:59:59,999
So if anyone wants to run for DPL you get
free beer, this is a good thing.

66
99:59:59,999 --> 99:59:59,999
But it did really impress on me that

67
99:59:59,999 --> 99:59:59,999
Debian's a big deal, it's a really huge
deal.

68
99:59:59,999 --> 99:59:59,999
If you have a look at some of the latest
server stats for web servers,

69
99:59:59,999 --> 99:59:59,999
Debian's number one.

70
99:59:59,999 --> 99:59:59,999
It's about 32% of Linux distributions, and
and if you combine Ubuntu as well, and

71
99:59:59,999 --> 99:59:59,999
all our derivatives, we're about 62% of
all linux servers out there

72
99:59:59,999 --> 99:59:59,999
Basically Debian really does run the world

73
99:59:59,999 --> 99:59:59,999
Not only that, but the amount of embedded
devices that Debian's involved with

74
99:59:59,999 --> 99:59:59,999
from, as mentioned before, the HP talk,
running huge teleco systems

75
99:59:59,999 --> 99:59:59,999
to assistive devices. I know Andy's around
somewhere.

76
99:59:59,999 --> 99:59:59,999
That product is essentially being based on
Debian and it's a speak and spell type

77
99:59:59,999 --> 99:59:59,999
device.

78
99:59:59,999 --> 99:59:59,999
It's for people who can't talk.

79
99:59:59,999 --> 99:59:59,999
You have a little keyboard, you type in
what you want to say, it has predictive

80
99:59:59,999 --> 99:59:59,999
technology in there, and then it gives
people a voice.

81
99:59:59,999 --> 99:59:59,999
Debian is literally being used to give
people voices who can't speak.

82
99:59:59,999 --> 99:59:59,999
This is the sort of impact that Debian
have, and free software can have

83
99:59:59,999 --> 99:59:59,999
on the world.

84
99:59:59,999 --> 99:59:59,999
A few things happened, certainly over the
last year.

85
99:59:59,999 --> 99:59:59,999
Apparently we released.

86
99:59:59,999 --> 99:59:59,999
I was only DPL for about a week, but I'm
going to take credit for this

87
99:59:59,999 --> 99:59:59,999
like any good politician, anyway.
[laughter]

88
99:59:59,999 --> 99:59:59,999
I've already had a stable release.

89
99:59:59,999 --> 99:59:59,999
This has hugely welcomed.

90
99:59:59,999 --> 99:59:59,999
I don't know if anyone followed the DevOps
Reactions tumblr page, but they were

91
99:59:59,999 --> 99:59:59,999
following along and huge cheers from
everyone when Debian releases.

92
99:59:59,999 --> 99:59:59,999
It is a big deal.

93
99:59:59,999 --> 99:59:59,999
Strangely I also saw a press release that
that said they're

94
99:59:59,999 --> 99:59:59,999
having a party to celebrate the release
of Debian 8 at Linux Fest North West,

95
99:59:59,999 --> 99:59:59,999
But this press release was from Microsoft
[laughter]

96
99:59:59,999 --> 99:59:59,999
I thought it might be a spoof at first,
but I diligently checked the certificates

97
99:59:59,999 --> 99:59:59,999
and domains and made sure it went back.

98
99:59:59,999 --> 99:59:59,999
I think people, certainly large
organisations are realising now that this

99
99:59:59,999 --> 99:59:59,999
open source, free software thing isn't
going away

100
99:59:59,999 --> 99:59:59,999
This isn't something that they can just
ignore, or they can fight against.

101
99:59:59,999 --> 99:59:59,999
This is something that they have to
embrace

102
99:59:59,999 --> 99:59:59,999
Certainly for someone like Microsoft to
throw away a load of cake and do a press

103
99:59:59,999 --> 99:59:59,999
release because Debian's released is
something I never thought I'd see when I

104
99:59:59,999 --> 99:59:59,999
first joined the Debian Project.

105
99:59:59,999 --> 99:59:59,999
We've had some new good things which have
started recently as well

106
99:59:59,999 --> 99:59:59,999
Fantastic areas if anyone knows what this
might be for a current initiative that's

107
99:59:59,999 --> 99:59:59,999
going on?

108
99:59:59,999 --> 99:59:59,999
[Picture of four similar potted plants]

109
99:59:59,999 --> 99:59:59,999
I was trying to do reproducible builds.

110
99:59:59,999 --> 99:59:59,999
They all sort of look the same, so near
enough [laughter]

111
99:59:59,999 --> 99:59:59,999
It's the nearest I could find on Flickr to
something being reproducible.

112
99:59:59,999 --> 99:59:59,999
These are all CC by the way.

113
99:59:59,999 --> 99:59:59,999
So this is perhaps a bit of a better slide
to explain just how impressive it is

114
99:59:59,999 --> 99:59:59,999
where we've got.

115
99:59:59,999 --> 99:59:59,999
I don't know if everyone here's aware of
reproducible builds and what this is

116
99:59:59,999 --> 99:59:59,999
trying to do, and the importance of it.

117
99:59:59,999 --> 99:59:59,999
When you get a source package

118
99:59:59,999 --> 99:59:59,999
and you produce a binary from that

119
99:59:59,999 --> 99:59:59,999
there hasn't traditionally been a way of
knowing that what you've produced here

120
99:59:59,999 --> 99:59:59,999
comes from this source package and it
hasn't been tampered with.

121
99:59:59,999 --> 99:59:59,999
This is incredibly important for the trust
that people have in Debian

122
99:59:59,999 --> 99:59:59,999
and how we produce things

123
99:59:59,999 --> 99:59:59,999
So if we're able to say "Look, this thing
here has definitely come from here,

124
99:59:59,999 --> 99:59:59,999
look we've rebuilt it again, you can check
for yourself, it comes from here.",

125
99:59:59,999 --> 99:59:59,999
then people can trust Debian as this
platform for where we run everyone's

126
99:59:59,999 --> 99:59:59,999
computers.

127
99:59:59,999 --> 99:59:59,999
I'm quite impressed with the remarkable
progress we've seen here.

128
99:59:59,999 --> 99:59:59,999
From zero to a huge share of things being
reproducable and that work I'm sure will

129
99:59:59,999 --> 99:59:59,999
continue, especially thanks to the Linux
Foundation's grants as well in supporting

130
99:59:59,999 --> 99:59:59,999
this progress.

131
99:59:59,999 --> 99:59:59,999
I did a "ask me anything" recently and it
was one of the things that came up as

132
99:59:59,999 --> 99:59:59,999
being a hugely popular thing that Debian
is doing and that we're driving forward.

133
99:59:59,999 --> 99:59:59,999
Not just for Debian itself but for all
distributions and making sure we're

134
99:59:59,999 --> 99:59:59,999
able to do that.

135
99:59:59,999 --> 99:59:59,999
Interestingly I was also asked what I'm
most jealous of other distributions and

136
99:59:59,999 --> 99:59:59,999
I think I said the Arch wiki because it is
pretty good. [laughter]

137
99:59:59,999 --> 99:59:59,999
Often when I'm on #debian and answering
questions then it comes up with the best

138
99:59:59,999 --> 99:59:59,999
answers a lot of the time.

139
99:59:59,999 --> 99:59:59,999
But, hey, I'm rubbish at writing
documentation so...

140
99:59:59,999 --> 99:59:59,999
Another effort we've come up with is ddebs

141
99:59:59,999 --> 99:59:59,999
Ability to automatically have debug
symbols

142
99:59:59,999 --> 99:59:59,999
Something which a few other distributions
have had for a while and it's really,

143
99:59:59,999 --> 99:59:59,999
really good to see that this sort of
effort is happening as well.

144
99:59:59,999 --> 99:59:59,999
If Niels is around.... well done!
[applause]

145
99:59:59,999 --> 99:59:59,999
Not the only one but.

146
99:59:59,999 --> 99:59:59,999
So, what's next?

147
99:59:59,999 --> 99:59:59,999
What's the next things that Debian can do?

148
99:59:59,999 --> 99:59:59,999
Where can we go from here and what sort of
ideas can we have?

149
99:59:59,999 --> 99:59:59,999
There's a whole range of things we can do

150
99:59:59,999 --> 99:59:59,999
but I'm just going to pick up two or three

151
99:59:59,999 --> 99:59:59,999
that we want to kinda concentrate on and
see where we're going.

152
99:59:59,999 --> 99:59:59,999
First, PPAs. [picture of a parcel]

153
99:59:59,999 --> 99:59:59,999
It's near enough a package, that'll do me.

154
99:59:59,999 --> 99:59:59,999
I've got a BoF scheduled on Friday, to try
and look at what we're doing with this and

155
99:59:59,999 --> 99:59:59,999
trying to finish it off.

156
99:59:59,999 --> 99:59:59,999
It was in my platform as something I want
push and it's something I believe will

157
99:59:59,999 --> 99:59:59,999
really help the development of Debian.

158
99:59:59,999 --> 99:59:59,999
Now, it's slightly different from Ubuntu
PPAs as they're well known.

159
99:59:59,999 --> 99:59:59,999
It's not going to be somewhere that you
can just dump random software and people

160
99:59:59,999 --> 99:59:59,999
install various quality packages.

161
99:59:59,999 --> 99:59:59,999
This is going to be a very useful tool to
aid Debian development itself.

162
99:59:59,999 --> 99:59:59,999
As far as I remember, most of the work is
actually done now.

163
99:59:59,999 --> 99:59:59,999
Huge thanks to the FTP Masters and DSA
etc for this.

164
99:59:59,999 --> 99:59:59,999
So the actual code is there in DAK.

165
99:59:59,999 --> 99:59:59,999
The only missing bits is the control
functions,

166
99:59:59,999 --> 99:59:59,999
how you create new PPAs and the wannabuild
system and we build stuff and

167
99:59:59,999 --> 99:59:59,999
touch releases.

168
99:59:59,999 --> 99:59:59,999
So it is going to, hopefully, come any
minute now

169
99:59:59,999 --> 99:59:59,999
and something that we will hopefully be
able to use and will ease the,

170
99:59:59,999 --> 99:59:59,999
sometimes the pain of when we freeze.

171
99:59:59,999 --> 99:59:59,999
Sometimes the ability to easily create
backports

172
99:59:59,999 --> 99:59:59,999
or even to ease library transitions.

173
99:59:59,999 --> 99:59:59,999
If you can create a PPA where you stage
your library, check everything works and

174
99:59:59,999 --> 99:59:59,999
you can fix all your breakages then that
should help unstable and testing as well.

175
99:59:59,999 --> 99:59:59,999
One on outreach I guess is near enough.

176
99:59:59,999 --> 99:59:59,999
It's kinda interesting, I've mentioned
that Debian is in a huge position to touch

177
99:59:59,999 --> 99:59:59,999
many people's lives and it was slightly
worrying that compared to the amount of

178
99:59:59,999 --> 99:59:59,999
Jessie release parties we had all over the
world,

179
99:59:59,999 --> 99:59:59,999
I video called into one in India for
example and it was globally popular.

180
99:59:59,999 --> 99:59:59,999
But all of our sprints have been in
Europe.

181
99:59:59,999 --> 99:59:59,999
So we haven't had some in North America.

182
99:59:59,999 --> 99:59:59,999
We haven't had any in South America.

183
99:59:59,999 --> 99:59:59,999
And there's huge areas here we can really
try and push and bring free software

184
99:59:59,999 --> 99:59:59,999
and really help push Debian to be the go
to place in various countries rather

185
99:59:59,999 --> 99:59:59,999
thank keeping it a Europe or sometimes
even North American market.

186
99:59:59,999 --> 99:59:59,999
And Debian's in a great place because we
are a community distribution.

187
99:59:59,999 --> 99:59:59,999
We try and aim to be the universal
operating system.

188
99:59:59,999 --> 99:59:59,999
Someone that anyone can come along, join,
help out in whatever way they want to,

189
99:59:59,999 --> 99:59:59,999
be it packaging, or doing wonderful press
work for example.

190
99:59:59,999 --> 99:59:59,999
[pointed look to the side] Not mentioning
that press work at all.

191
99:59:59,999 --> 99:59:59,999
She's not even looking at me.
[Francesca] Hey!

192
99:59:59,999 --> 99:59:59,999
[Laughter]

193
99:59:59,999 --> 99:59:59,999
[Neil] She's going to volunteer to do more
press work in future again.

194
99:59:59,999 --> 99:59:59,999
So it's an area people can get involved in
really easily and because we're

195
99:59:59,999 --> 99:59:59,999
distributed, because we work online, it is
natural that people can come and join us

196
99:59:59,999 --> 99:59:59,999
and we should encourage that, and we
should try and reach out and try and reach

197
99:59:59,999 --> 99:59:59,999
more people.

198
99:59:59,999 --> 99:59:59,999
Third area, the final one here is around
accessibility.

199
99:59:59,999 --> 99:59:59,999
It's kind of a sad fact that free software
is about 10 years behind proprietary and

200
99:59:59,999 --> 99:59:59,999
commercial offerings.

201
99:59:59,999 --> 99:59:59,999
There's a reason that iPhones are hugely
popular with people who need accessibility

202
99:59:59,999 --> 99:59:59,999
options - they are just fantastic compared
with what you can get normally.

203
99:59:59,999 --> 99:59:59,999
I don't believe that it is right that
people should have to use proprietary

204
99:59:59,999 --> 99:59:59,999
software, which they pay for, to access
computing, and to access the web, and to

205
99:59:59,999 --> 99:59:59,999
be able to explore what we all have.

206
99:59:59,999 --> 99:59:59,999
We should be able to put in effort, so
there's certainly an area which we can

207
99:59:59,999 --> 99:59:59,999
really make a difference to people and try
and drive forward.

208
99:59:59,999 --> 99:59:59,999
To bring computing to everyone, rather
than those who are fortunate enough to be

209
99:59:59,999 --> 99:59:59,999
able to see a screen well, or be rich
enough to be able to pay for a license or

210
99:59:59,999 --> 99:59:59,999
buy a computer or system from a
proprietary company.

211
99:59:59,999 --> 99:59:59,999
There is no reason why we shouldn't be
able to do this.

212
99:59:59,999 --> 99:59:59,999
We are able to speak and work directly
with people who require accessibility

213
99:59:59,999 --> 99:59:59,999
features, and let those people design it
in a way that is much easier than any

214
99:59:59,999 --> 99:59:59,999
proprietary company.

215
99:59:59,999 --> 99:59:59,999
So it's an area I think we can try and
try and sort of push quite a bit.

216
99:59:59,999 --> 99:59:59,999
Finally, a bit of a huge thank you from me
really.

217
99:59:59,999 --> 99:59:59,999
It was about... So I've been involved with
Debian since about 2001 or so, and doing

218
99:59:59,999 --> 99:59:59,999
loads of different roles.

219
99:59:59,999 --> 99:59:59,999
Originally doing the web apps policy,
which never really got beyond draft

220
99:59:59,999 --> 99:59:59,999
because web apps are terrible, terrible
things that don't really work with

221
99:59:59,999 --> 99:59:59,999
distributions that well.

222
99:59:59,999 --> 99:59:59,999
Then with Joey Hess, doing testing
security team and setting up that,

223
99:59:59,999 --> 99:59:59,999
eventually that led in to many, many years
of release management. [shakes head]

224
99:59:59,999 --> 99:59:59,999
[heckling]

225
99:59:59,999 --> 99:59:59,999
And then many years of being release
manager as well.

226
99:59:59,999 --> 99:59:59,999
And then after that Tolimar found me at
DebConf in Banja Luka and found out I

227
99:59:59,999 --> 99:59:59,999
could write a press release and roped me
in to doing bits there

228
99:59:59,999 --> 99:59:59,999
but after that I was sort of feeling a
bit, I don't know, burned out.

229
99:59:59,999 --> 99:59:59,999
I didn't really know what else to do with
the project.

230
99:59:59,999 --> 99:59:59,999
I think this happens to everyone.

231
99:59:59,999 --> 99:59:59,999
It's a huge reminder each time when I come
to DebConf and I meet people and how

232
99:59:59,999 --> 99:59:59,999
fortunate I am, and we all are to be
involved with such a fantastic project,

233
99:59:59,999 --> 99:59:59,999
something that really is changing people's
lives,

234
99:59:59,999 --> 99:59:59,999
something that is breaking the
traditional proprietary market

235
99:59:59,999 --> 99:59:59,999
and enabling people to have greater access
to computing

236
99:59:59,999 --> 99:59:59,999
and it's really fortunate that I'm in a
position to help lead this project

237
99:59:59,999 --> 99:59:59,999
and to do everything for you.

238
99:59:59,999 --> 99:59:59,999
Finally, I think I mentioned, I think Phil
Hands - is he around?

239
99:59:59,999 --> 99:59:59,999
He probably has a small child trying to
throw-up on him for anyone who saw the

240
99:59:59,999 --> 99:59:59,999
morning announcements today.

241
99:59:59,999 --> 99:59:59,999
He did kind of joke about the next DPL
hustings,

242
99:59:59,999 --> 99:59:59,999
he can just write an auto-responder bot to
any questions which is

243
99:59:59,999 --> 99:59:59,999
"That sounds interesting, I look forward
to seeing the results"

244
99:59:59,999 --> 99:59:59,999
[laughter]

245
99:59:59,999 --> 99:59:59,999
Generally I think that we should be able
to try things,

246
99:59:59,999 --> 99:59:59,999
so if anyone has any ideas, stuff they
want to do, Debian has the money,

247
99:59:59,999 --> 99:59:59,999
let's go try some stuff.

248
99:59:59,999 --> 99:59:59,999
If we want a sprint to work on
accessibility in say, Hong Hong

249
99:59:59,999 --> 99:59:59,999
as there's a huge issue there, or to
improve our localisation, then let's

250
99:59:59,999 --> 99:59:59,999
do that.

251
99:59:59,999 --> 99:59:59,999
We have the money to do it.

252
99:59:59,999 --> 99:59:59,999
We have, hopefully, the interest around,
so let's go try things.

253
99:59:59,999 --> 99:59:59,999
As promised, I thought I'd keep things
nice and short.

254
99:59:59,999 --> 99:59:59,999
As everyone seemed to be getting hungry.

255
99:59:59,999 --> 99:59:59,999
I was going to leave a bit for Q&amp;A, if
anyone had any questions,

256
99:59:59,999 --> 99:59:59,999
or wanted to put me on the spot about
anything or random thoughts anyone had,

257
99:59:59,999 --> 99:59:59,999
then I'd certainly be very happy to answer
any of those.

258
99:59:59,999 --> 99:59:59,999
Someone has to be first.

259
99:59:59,999 --> 99:59:59,999
Otherwise my timing's really, really out.

260
99:59:59,999 --> 99:59:59,999
Oh good. [clappiung]

261
99:59:59,999 --> 99:59:59,999
Lucky me.

262
99:59:59,999 --> 99:59:59,999
Hello! What's your name and where are you
from? [laughter]

263
99:59:59,999 --> 99:59:59,999
[Steve] Oh, you want me to say something?
[Neil] Yeah, go on.

264
99:59:59,999 --> 99:59:59,999
[Steve] What is your single highest
priority thing that you think we should

265
99:59:59,999 --> 99:59:59,999
all be working on?

266
99:59:59,999 --> 99:59:59,999
[Neil] Getting the next stable release
out, to be honest.

267
99:59:59,999 --> 99:59:59,999
This is what we do, as a distribution.

268
99:59:59,999 --> 99:59:59,999
We release things.

269
99:59:59,999 --> 99:59:59,999
We make software.

270
99:59:59,999 --> 99:59:59,999
We give it to our users, Debian is famed
for its stability.

271
99:59:59,999 --> 99:59:59,999
We backport our security fixes.

272
99:59:59,999 --> 99:59:59,999
People can rely on Debian, they can trust
us to produce a rock solid distribution.

273
99:59:59,999 --> 99:59:59,999
Something that people can, in some cases,
yes, derive works from.

274
99:59:59,999 --> 99:59:59,999
If they don't like what we're doing they
can tweak it.

275
99:59:59,999 --> 99:59:59,999
There was the huge thing that came up when
Devuan came up saying that

276
99:59:59,999 --> 99:59:59,999
"Oh, we're going to fork Debian and it's
going to be terrible"

277
99:59:59,999 --> 99:59:59,999
It's like fine! There's over 120 forks of
Debian out there already, that's fine!

278
99:59:59,999 --> 99:59:59,999
Please, come and do this, we're happy with
that.

279
99:59:59,999 --> 99:59:59,999
Being able to produce this reliable,
stable operating system that we only

280
99:59:59,999 --> 99:59:59,999
release when we're ready, which happens to
apparently be about every two years now

281
99:59:59,999 --> 99:59:59,999
is something that everyone relies on us
being able to do,

282
99:59:59,999 --> 99:59:59,999
and they can trust us to produce that for
them.

283
99:59:59,999 --> 99:59:59,999
Essentially putting out releases is one of
the main reasons we're here.

284
99:59:59,999 --> 99:59:59,999
To get that software into the hands of
users.

285
99:59:59,999 --> 99:59:59,999
There's loads of other stuff of course
which we can do to try and improve and

286
99:59:59,999 --> 99:59:59,999
push forward free software in general, but
we are essentially a distribution.

287
99:59:59,999 --> 99:59:59,999
Our aim is to collect software, and then
distribute it,

288
99:59:59,999 --> 99:59:59,999
and one of the best ways to do that is
releases.

289
99:59:59,999 --> 99:59:59,999
[Steve] Good answer.
[Neil] Good!

290
99:59:59,999 --> 99:59:59,999
[Joshua] What's your favourite thing about
Debian?

291
99:59:59,999 --> 99:59:59,999
[laughter]

292
99:59:59,999 --> 99:59:59,999
[Unknown] You're not allowed to say
releases.

293
99:59:59,999 --> 99:59:59,999
[Neil] I'm apparently not allowed to say
the kilt as well

294
99:59:59,999 --> 99:59:59,999
[Bdale] You also already said free beer so

295
99:59:59,999 --> 99:59:59,999
[Neil] And free beer, yeah.

296
99:59:59,999 --> 99:59:59,999
It has to be the people that I'm involved
with.

297
99:59:59,999 --> 99:59:59,999
The project has enabled me to meet so
many awesome people and basically have

298
99:59:59,999 --> 99:59:59,999
the career I've had so far as well.

299
99:59:59,999 --> 99:59:59,999
There has always been a thing about being
involved visibly with and open source

300
99:59:59,999 --> 99:59:59,999
project that helps you professionally as
well.

301
99:59:59,999 --> 99:59:59,999
I got my first job because I was involved
with free software.

302
99:59:59,999 --> 99:59:59,999
I then got my next job because I was a
Debian developer and could put together

303
99:59:59,999 --> 99:59:59,999
Linux systems easily, and knew how to
munge these various crazy different

304
99:59:59,999 --> 99:59:59,999
projects which are written in a hundred
different libraries,

305
99:59:59,999 --> 99:59:59,999
using different compatibilities together
to make something whole.

306
99:59:59,999 --> 99:59:59,999
Then my current job I got because
essentially I was involved in doing

307
99:59:59,999 --> 99:59:59,999
management-y type functions in Debian.

308
99:59:59,999 --> 99:59:59,999
As I say to people when they ask

309
99:59:59,999 --> 99:59:59,999
"Where's the money in free software? How
can I create a career with it?"

310
99:59:59,999 --> 99:59:59,999
It's basically fairly easy. Get stuck in.

311
99:59:59,999 --> 99:59:59,999
Go and do something.

312
99:59:59,999 --> 99:59:59,999
Find something interesting that interests
you,

313
99:59:59,999 --> 99:59:59,999
and that you're able to be the world
expert at, and you can do that.

314
99:59:59,999 --> 99:59:59,999
On an entirely personal view, some of my
closest friends are the Debian people I've

315
99:59:59,999 --> 99:59:59,999
worked with for many many years and I
certainly wouldn't have moved to my

316
99:59:59,999 --> 99:59:59,999
current city, or know half the people I do
if it wasn't for that.

317
99:59:59,999 --> 99:59:59,999
The Debian family is a large one, that
occasionally gets together at DebConf

318
99:59:59,999 --> 99:59:59,999
and Christmas, and sometimes has huge blow
up arguments,

319
99:59:59,999 --> 99:59:59,999
possibly over the turkey or the init
system, something like that [laughter]

320
99:59:59,999 --> 99:59:59,999
but we do all come together.

321
99:59:59,999 --> 99:59:59,999
We have huge contentious decisions

322
99:59:59,999 --> 99:59:59,999
but we're still here.

323
99:59:59,999 --> 99:59:59,999
We've had huge arguments with everyone
trying to stab each other, but at the end

324
99:59:59,999 --> 99:59:59,999
of the day we're still here, we're still
the Debian project.

325
99:59:59,999 --> 99:59:59,999
And sort of like that it's something we
love to be associated with

326
99:59:59,999 --> 99:59:59,999
and it's everyone around that really makes
that.

327
99:59:59,999 --> 99:59:59,999
[Joshua] Thank you
[Applause]

328
99:59:59,999 --> 99:59:59,999
[Neil] Anyone else? No?

329
99:59:59,999 --> 99:59:59,999
Oh oh.

330
99:59:59,999 --> 99:59:59,999
I'll just stop you there - backups are
really really important, [laughter]

331
99:59:59,999 --> 99:59:59,999
so we should definitely concentrate on
backups.

332
99:59:59,999 --> 99:59:59,999
[Lars] So what's the biggest risk to
Debian you see in the 5-10 year span?

333
99:59:59,999 --> 99:59:59,999
[[[25:48]]]
