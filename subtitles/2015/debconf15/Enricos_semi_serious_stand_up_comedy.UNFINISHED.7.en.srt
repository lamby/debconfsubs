1
00:00:00,522 --> 00:00:01,951
there is a bug

2
00:00:01,951 --> 00:00:05,650
I can not realy on clicking on the terminal to open a link in a browser

3
00:00:05,650 --> 00:00:08,871
because there is currently a bug in konsole with a k

4
00:00:08,871 --> 00:00:12,441
that opens link in random browsers

5
00:00:14,761 --> 00:00:19,002
and i learned that i have a browser installed that i didnt know existed

6
00:00:19,012 --> 00:00:22,560
its called eric6

7
00:00:22,621 --> 00:00:30,261
and if you have the eric python
graphical development environment installed

8
00:00:31,041 --> 00:00:38,240
it contains a browser and it maps it to mine

9
00:00:38,240 --> 00:00:43,001
and if you have a terminal that for some reason open browsers at random

10
00:00:43,001 --> 00:00:47,101
you get links to open in that

11
00:00:47,101 --> 00:00:49,492
so ill copy and paste the urls

12
00:00:49,492 --> 00:00:51,221
it will be slighly slower

13
00:00:51,221 --> 00:00:53,900
i think i have 5 minutes still to go

14
00:00:58,040 --> 00:01:01,731
time passes quickly when you have fun

15
00:01:04,601 --> 00:01:07,602
audience: install terminator!

16
00:01:07,602 --> 00:01:09,010
enrico: no

17
00:01:09,010 --> 00:01:14,041
if look at my blog, there is a list of my
requirements for terminal emulators

18
00:01:14,041 --> 00:01:17,081
and the reason they all suck

19
00:01:17,081 --> 00:01:20,062
all the bits that do not work

20
00:01:20,062 --> 00:01:24,421
now terminator is in that list,
it has things that dont work for me

21
00:01:24,421 --> 00:01:27,861
but we have the upstream for
lxterminal at the conference

22
00:01:27,861 --> 00:01:31,102
that has seen the list and went like

23
00:01:31,102 --> 00:01:33,781
that i implemented 2 days ago

24
00:01:33,781 --> 00:01:35,541
that is in my todo list now

25
00:01:35,541 --> 00:01:39,321
that, let me have a look at the code,
yes i can do it

26
00:01:39,321 --> 00:01:41,211
im working on it

27
00:01:41,211 --> 00:01:44,301
and so thank you

28
00:01:59,272 --> 00:02:01,880
3 minutes i believe

29
00:02:01,880 --> 00:02:02,701
2

30
00:02:06,892 --> 00:02:09,602
well, in the meantime, i will read something

31
00:02:09,602 --> 00:02:13,921
that i guess everyone 
should had read before entering

32
00:02:13,921 --> 00:02:17,841
which is, i will ramble freely
about debian

33
00:02:17,871 --> 00:02:20,181
and everything else i care about

34
00:02:20,181 --> 00:02:23,721
and you are going to have the misfortune

35
00:02:24,041 --> 00:02:27,842
of figuring out what it is that 
i also care about

36
00:02:29,632 --> 00:02:32,522
i will cover topics including

37
00:02:33,002 --> 00:02:35,791
but specially, not limited to,

38
00:02:36,022 --> 00:02:39,041
anarchism, relationships, sex, violence

39
00:02:39,041 --> 00:02:42,350
society, stereotypes and expectations

40
00:02:42,350 --> 00:02:46,920
and i will always be talking about debian

41
00:02:47,441 --> 00:02:49,212
really

42
00:02:49,212 --> 00:02:51,780
i expect that this talks will be both

43
00:02:51,780 --> 00:02:54,791
unsuitable and insightful for pretty much

44
00:02:55,211 --> 00:02:58,621
any kind of audience that i can think of

45
00:02:58,691 --> 00:03:03,341
and first 3 minutes of it are
about making sure

46
00:03:03,360 --> 00:03:06,800
you understand what will be
the remaining 27

47
00:03:07,040 --> 00:03:11,480
and have a chance to leave

48
00:03:20,601 --> 00:03:23,421
after 3 minutes if you leave

49
99:59:59,999 --> 99:59:59,999
you are kinda seen by everyone or something

50
99:59:59,999 --> 99:59:59,999
but some shy people might

51
99:59:59,999 --> 99:59:59,999
i dont know

52
99:59:59,999 --> 99:59:59,999
no, at any point in the talk

53
99:59:59,999 --> 99:59:59,999
if anybody want to safe word out

54
99:59:59,999 --> 99:59:59,999
and ill explain what the safe word is

55
99:59:59,999 --> 99:59:59,999
but if anybody want out and

56
99:59:59,999 --> 99:59:59,999
lalala i dont want to hear anymore

57
99:59:59,999 --> 99:59:59,999
raise your hand, i will stop

58
99:59:59,999 --> 99:59:59,999
black the screen

59
99:59:59,999 --> 99:59:59,999
?? my ?? file will show up in my clumsy attempt

60
99:59:59,999 --> 99:59:59,999
to black the screen, and waiting for you to leave

61
99:59:59,999 --> 99:59:59,999
the room before resuming, so

62
99:59:59,999 --> 99:59:59,999
??

63
99:59:59,999 --> 99:59:59,999
quotations that are famous anarchists have said

64
99:59:59,999 --> 99:59:59,999
hundreds of years ago about debian

65
99:59:59,999 --> 99:59:59,999
like this could be about mailing list discussions

66
99:59:59,999 --> 99:59:59,999
for example

67
99:59:59,999 --> 99:59:59,999
i guess i can begin

68
99:59:59,999 --> 99:59:59,999
chapter one: disclaimers

69
99:59:59,999 --> 99:59:59,999
sorry, I was supposed to be introduced

70
99:59:59,999 --> 99:59:59,999
I was born...

71
99:59:59,999 --> 99:59:59,999
is it on? okay

72
99:59:59,999 --> 99:59:59,999
it gives me great pleasure to introduce a man that

73
99:59:59,999 --> 99:59:59,999
that pushed me into the maintainers process about

74
99:59:59,999 --> 99:59:59,999
seven years ago

75
99:59:59,999 --> 99:59:59,999
Enrico Zini

76
99:59:59,999 --> 99:59:59,999
so, chapter one: disclaimers

77
99:59:59,999 --> 99:59:59,999
i will let people know my better introducer

78
99:59:59,999 --> 99:59:59,999
Aida: anyhow if you are still a little dark

79
99:59:59,999 --> 99:59:59,999
in the end, just ask a young person or google

80
99:59:59,999 --> 99:59:59,999
i mean, dont take your computer for a ??

81
99:59:59,999 --> 99:59:59,999
i mean ?? afterwards

82
99:59:59,999 --> 99:59:59,999
Enrico: that songs talks about dogging

83
99:59:59,999 --> 99:59:59,999
which is an interesting topic to kind of give

84
99:59:59,999 --> 99:59:59,999
an idea about the rest of the thing

85
99:59:59,999 --> 99:59:59,999
its good to give you an idea about the level

86
99:59:59,999 --> 99:59:59,999
of the rest of the discussion

87
99:59:59,999 --> 99:59:59,999
so you know what is going to happen

88
99:59:59,999 --> 99:59:59,999
dogging redirect to public sex

89
99:59:59,999 --> 99:59:59,999
its one of the famous

90
99:59:59,999 --> 99:59:59,999
anyone safewording outs?

91
99:59:59,999 --> 99:59:59,999
you okay?

92
99:59:59,999 --> 99:59:59,999
its a british thing apparently

93
99:59:59,999 --> 99:59:59,999
a british are famous for steam engines

94
99:59:59,999 --> 99:59:59,999
and dogging and a bunch of other things

95
99:59:59,999 --> 99:59:59,999
hot and cold water, ?? but dogging in this case

96
99:59:59,999 --> 99:59:59,999
and dogging like debian has

97
99:59:59,999 --> 99:59:59,999
a code of conduct

98
99:59:59,999 --> 99:59:59,999
its good that we found a liquor tap

99
99:59:59,999 --> 99:59:59,999
which people who are doing things seriously

100
99:59:59,999 --> 99:59:59,999
there is more if you goggle

101
99:59:59,999 --> 99:59:59,999
but yeah, its always sex in public spaces

102
99:59:59,999 --> 99:59:59,999
its usually done with cars

103
99:59:59,999 --> 99:59:59,999
in cars, im not sure someone might

104
99:59:59,999 --> 99:59:59,999
but i personally wouldnt recommend it

105
99:59:59,999 --> 99:59:59,999
its not my thing

106
99:59:59,999 --> 99:59:59,999
theres a code that if you would like to be seen

107
99:59:59,999 --> 99:59:59,999
while you do it, you leave the light on

108
99:59:59,999 --> 99:59:59,999
inside the car

109
99:59:59,999 --> 99:59:59,999
if you want to be followed into another's place

110
99:59:59,999 --> 99:59:59,999
you flash your lights

111
99:59:59,999 --> 99:59:59,999
i think there was a thing about raising the windows

112
99:59:59,999 --> 99:59:59,999
but im not sure about that

113
99:59:59,999 --> 99:59:59,999
theres a wonderful documental from BBC

114
99:59:59,999 --> 99:59:59,999
where you can find

115
99:59:59,999 --> 99:59:59,999
ask the british

116
99:59:59,999 --> 99:59:59,999
so this set

117
99:59:59,999 --> 99:59:59,999
Aida: theres a sense of dispossition

118
99:59:59,999 --> 99:59:59,999
leave now

119
99:59:59,999 --> 99:59:59,999
right, we can begin

120
99:59:59,999 --> 99:59:59,999
about the debian social contract

121
99:59:59,999 --> 99:59:59,999
and Emma Goldman said once that

122
99:59:59,999 --> 99:59:59,999
Every daring attempt to make a great change

123
99:59:59,999 --> 99:59:59,999
in existing conditions, every lofty vision of

124
99:59:59,999 --> 99:59:59,999
new possibilities for the human race

125
99:59:59,999 --> 99:59:59,999
has been labeled Utopian

126
99:59:59,999 --> 99:59:59,999
we do

127
99:59:59,999 --> 99:59:59,999
im going to talk about many topics

128
99:59:59,999 --> 99:59:59,999
that we all know have so much in common

129
99:59:59,999 --> 99:59:59,999
Anarchism, poliamory, BSDM and free software

130
99:59:59,999 --> 99:59:59,999
and after all, they are about people

131
99:59:59,999 --> 99:59:59,999
consensually, doing things together, right?

132
99:59:59,999 --> 99:59:59,999
people consensually doing things together

133
99:59:59,999 --> 99:59:59,999
so its different communities, with some overlaps

134
99:59:59,999 --> 99:59:59,999
they are different communities of people

135
99:59:59,999 --> 99:59:59,999
consensually doing things together and

136
99:59:59,999 --> 99:59:59,999
there a lot to talk, theres a lot to learn

137
99:59:59,999 --> 99:59:59,999
from each other, because for a reason

138
99:59:59,999 --> 99:59:59,999
or another all this communities are focus on

139
99:59:59,999 --> 99:59:59,999
different aspects of aspects of consensually

140
99:59:59,999 --> 99:59:59,999
doing things together

141
99:59:59,999 --> 99:59:59,999
anarchism might focus more on the political side

142
99:59:59,999 --> 99:59:59,999
BDSM might focus more on the consensual side

143
99:59:59,999 --> 99:59:59,999
and on the things that you do, free software

144
99:59:59,999 --> 99:59:59,999
and poliamory have a lot of focus on the political

145
99:59:59,999 --> 99:59:59,999
the doing things, the consensual and ?? everything else

146
99:59:59,999 --> 99:59:59,999
chapter 2: BDSM

147
99:59:59,999 --> 99:59:59,999
Lysander Spooner talking about proprietary cloud

148
99:59:59,999 --> 99:59:59,999
service providers said that

149
99:59:59,999 --> 99:59:59,999
A person is no less a slave because they are allowed

150
99:59:59,999 --> 99:59:59,999
to choose a new master once in a term of years

151
99:59:59,999 --> 99:59:59,999
so, BDSM

152
99:59:59,999 --> 99:59:59,999
we freesoftware people think that we are clever

153
99:59:59,999 --> 99:59:59,999
because we have recursive acronyms

154
99:59:59,999 --> 99:59:59,999
BDSM stands for

155
99:59:59,999 --> 99:59:59,999
Bondage disco is an interesting concept

156
99:59:59,999 --> 99:59:59,999
we could expanded later im sure it can go somewhere

157
99:59:59,999 --> 99:59:59,999
Bondage, Discipline, Dominance, Submission

158
99:59:59,999 --> 99:59:59,999
who was in the debian contributors this afternoon

159
99:59:59,999 --> 99:59:59,999
has worked on code that submits debian developers

160
99:59:59,999 --> 99:59:59,999
sorry, submit debian contributors to the site

161
99:59:59,999 --> 99:59:59,999
but its not the same thing

162
99:59:59,999 --> 99:59:59,999
so its a chained acronym

163
99:59:59,999 --> 99:59:59,999
BD, DS, SM (Bondage Discipline, Dominance Submission, Sado Masochism)

164
99:59:59,999 --> 99:59:59,999
so thats the first thing we have in common

165
99:59:59,999 --> 99:59:59,999
clever acronyms

166
99:59:59,999 --> 99:59:59,999
so its about some of those things

167
99:59:59,999 --> 99:59:59,999
i think BDSM is really interesting not just because

168
99:59:59,999 --> 99:59:59,999
of whips but also for having develop over the years

169
99:59:59,999 --> 99:59:59,999
a lot of awareness about power relationships

170
99:59:59,999 --> 99:59:59,999
its in the acronym, dominance-submission-discipline

171
99:59:59,999 --> 99:59:59,999
with that in mind i have a BDSM freesoftware definition

172
99:59:59,999 --> 99:59:59,999
i refuse to be bound by software

173
99:59:59,999 --> 99:59:59,999
i cannot negotiate with

174
99:59:59,999 --> 99:59:59,999
and how i negotiate with software

175
99:59:59,999 --> 99:59:59,999
well, if i have the source code i can come to terms

176
99:59:59,999 --> 99:59:59,999
with it and nogiating my interaction and if i dont

177
99:59:59,999 --> 99:59:59,999
the source code or i dont have the ability to change it

178
99:59:59,999 --> 99:59:59,999
then im stuck, right?

179
99:59:59,999 --> 99:59:59,999
so i think thats a pretty good definition of freesoftware

180
99:59:59,999 --> 99:59:59,999
im very proud of that

181
99:59:59,999 --> 99:59:59,999
so thats another way to coceptualize freesoftware that

182
99:59:59,999 --> 99:59:59,999
i think that can be understood really well by people

183
99:59:59,999 --> 99:59:59,999
who may struggle with the four freedoms or the 10 points

184
99:59:59,999 --> 99:59:59,999
of the ??

185
99:59:59,999 --> 99:59:59,999
so thats one thing that could be carried across

186
99:59:59,999 --> 99:59:59,999
theres another acronym that i would like to submit

187
99:59:59,999 --> 99:59:59,999
to your consideration which is this

188
99:59:59,999 --> 99:59:59,999
Your Kink Is Not My Kink But Your Kink Is Okay (YKINMKBYKIOK)

189
99:59:59,999 --> 99:59:59,999
which i said but i should expand it for those that dont

190
99:59:59,999 --> 99:59:59,999
find it obvious

191
99:59:59,999 --> 99:59:59,999
kink is something people do

192
99:59:59,999 --> 99:59:59,999
and something that some other people might not want to do

193
99:59:59,999 --> 99:59:59,999
in the BSDM community is costumary to say

194
99:59:59,999 --> 99:59:59,999
your kink is not my kink but your kink is okay

195
99:59:59,999 --> 99:59:59,999
the idea is to be non judgamental but at the same time

196
99:59:59,999 --> 99:59:59,999
to pull oneself out from somebody that one doesnt want to do

197
99:59:59,999 --> 99:59:59,999
in my opinion, this is the final solution

198
99:59:59,999 --> 99:59:59,999
for the VI vs EMACS problem

199
99:59:59,999 --> 99:59:59,999
im really eager to carry this into the freesoftware

200
99:59:59,999 --> 99:59:59,999
community, it would solve a lot of things

201
99:59:59,999 --> 99:59:59,999
other interesting things about BDSM is awareness about

202
99:59:59,999 --> 99:59:59,999
confort zones i know when im doing something im confortable with

203
99:59:59,999 --> 99:59:59,999
i know my limits i know the limits i have i do not want to push

204
99:59:59,999 --> 99:59:59,999
i learned to communicate all this

205
99:59:59,999 --> 99:59:59,999
otherwise dissaster happens

206
99:59:59,999 --> 99:59:59,999
there are safewords to prevent dissater to happen

207
99:59:59,999 --> 99:59:59,999
safeword means a word that means stop everything right now

208
99:59:59,999 --> 99:59:59,999
no matter what

209
99:59:59,999 --> 99:59:59,999
there is even level of safe words

210
99:59:59,999 --> 99:59:59,999
common standard traffic lights green, yellow, red

211
99:59:59,999 --> 99:59:59,999
green means yeah, bring it on

212
99:59:59,999 --> 99:59:59,999
yellow means this is getting intense, but do go on

213
99:59:59,999 --> 99:59:59,999
red means stop right now or i will call the police

214
99:59:59,999 --> 99:59:59,999
once i manage to manage to wiggle free of this

215
99:59:59,999 --> 99:59:59,999
incidentally safe words are something we dont have in debian

216
99:59:59,999 --> 99:59:59,999
and i would be interested to have

217
99:59:59,999 --> 99:59:59,999
there are things that i see in mailing lists that i would like

218
99:59:59,999 --> 99:59:59,999
to safeword out

219
99:59:59,999 --> 99:59:59,999
there is the clause the mail browser option yes

220
99:59:59,999 --> 99:59:59,999
it works pretty well, but then some people might take decisions

221
99:59:59,999 --> 99:59:59,999
and then... well, its a concept i think should be explored

222
99:59:59,999 --> 99:59:59,999
theres this nice gift that tell me that i can zoom, yes

223
99:59:59,999 --> 99:59:59,999
"No means no", unless there has been a long discussion first,

224
99:59:59,999 --> 99:59:59,999
and a safeword is in place, in which case "Fuzzy purple unicorn" means "no"

225
99:59:59,999 --> 99:59:59,999
thats the idea of safewords and "no means no" is an important

226
99:59:59,999 --> 99:59:59,999
concept in most of the BSDM community

227
99:59:59,999 --> 99:59:59,999
theres the some fringes in there but i think is unimportant

228
99:59:59,999 --> 99:59:59,999
theres an interesting thing coming from the BDSM comunity

229
99:59:59,999 --> 99:59:59,999
is the awereness that really, when no means no

230
99:59:59,999 --> 99:59:59,999
is easier to say yes

231
99:59:59,999 --> 99:59:59,999
if im around a person that says, would you like to have tea

232
99:59:59,999 --> 99:59:59,999
and i say mmm nooo?

233
99:59:59,999 --> 99:59:59,999
and i go like: but do have tea, tea is good

234
99:59:59,999 --> 99:59:59,999
and im like no

235
99:59:59,999 --> 99:59:59,999
and it goes: but its really good tea

236
99:59:59,999 --> 99:59:59,999
then i start feeling really unconfortable and chances are

237
99:59:59,999 --> 99:59:59,999
i dont want to drink anymore tea in my whole life

238
99:59:59,999 --> 99:59:59,999
which is a shame, because i might like tea

239
99:59:59,999 --> 99:59:59,999
incidentally, you can have consent explain with tea

240
99:59:59,999 --> 99:59:59,999
video: if you are still struggling with consent

241
99:59:59,999 --> 99:59:59,999
just imagine instead of iniciating sex, you are making them a cup of tea

242
99:59:59,999 --> 99:59:59,999
you said: hey, would you like a cup of tea?

243
99:59:59,999 --> 99:59:59,999
they go: omg, fuck yes, i would fucking love a cup of tea

244
99:59:59,999 --> 99:59:59,999
thank you

245
99:59:59,999 --> 99:59:59,999
then you know they want a cup of tea

246
99:59:59,999 --> 99:59:59,999
if you say: hey would you like a cup of tea?

247
99:59:59,999 --> 99:59:59,999
they are like: hmmm, you know, im not really sure

248
99:59:59,999 --> 99:59:59,999
but you could make him a cup of tea or not

249
99:59:59,999 --> 99:59:59,999
you could make him a cup of tea... or not

250
99:59:59,999 --> 99:59:59,999
but be aware they might not drink it

251
99:59:59,999 --> 99:59:59,999
and if they dont drink it, then if they dont drink it

252
99:59:59,999 --> 99:59:59,999
then, and this is the important part, dont make them drink it

253
99:59:59,999 --> 99:59:59,999
just because you made it, doenst mean you are entitled to watch them

254
99:59:59,999 --> 99:59:59,999
drink it

255
99:59:59,999 --> 99:59:59,999
and if they say: no thank you

256
99:59:59,999 --> 99:59:59,999
then, dont make them tea. That is all

257
99:59:59,999 --> 99:59:59,999
enrico: i highly recommend this video

258
99:59:59,999 --> 99:59:59,999
consent is important, ill come back to it later

259
99:59:59,999 --> 99:59:59,999
theres another thing that i really like about BDSM

260
99:59:59,999 --> 99:59:59,999
its a concept wich is aftercare

261
99:59:59,999 --> 99:59:59,999
the idea is that after you are having a person, bound... whatever

262
99:59:59,999 --> 99:59:59,999
and you both have push your comfort
zones a little further than usual

263
99:59:59,999 --> 99:59:59,999
when the whole thing ends, there's a
moment of recovering

264
99:59:59,999 --> 99:59:59,999
getting back inside the comfort zones of love,
of celebration

265
99:59:59,999 --> 99:59:59,999
which is called aftercare, which is
something

266
99:59:59,999 --> 99:59:59,999
that we dont do much in debian

267
99:59:59,999 --> 99:59:59,999
we do debian release parties. Aftercare for release

268
99:59:59,999 --> 99:59:59,999
we are out of the bindings of the free spirit

269
99:59:59,999 --> 99:59:59,999
and then we get back into the comfort zone

270
99:59:59,999 --> 99:59:59,999
of freedom and gcc transition and breaking
unstable

271
99:59:59,999 --> 99:59:59,999
but there could be moments of celebration

272
99:59:59,999 --> 99:59:59,999
after each vote on something

273
99:59:59,999 --> 99:59:59,999
there could be solidarity towards the people

274
99:59:59,999 --> 99:59:59,999
that werent represented on the results
of the election

275
99:59:59,999 --> 99:59:59,999
or high fives after springs and
lots of efforts

276
99:59:59,999 --> 99:59:59,999
thats another thing that, in part we do,

277
99:59:59,999 --> 99:59:59,999
but could do more, because more hacks
is nice

278
99:59:59,999 --> 99:59:59,999
except if one does not like hacks

279
99:59:59,999 --> 99:59:59,999
at first, approach people at front
if you want to hug

280
99:59:59,999 --> 99:59:59,999
and signal that you want a hug, 
before entering their personal space

281
99:59:59,999 --> 99:59:59,999
do not hug people from the back,
that short of things

282
99:59:59,999 --> 99:59:59,999
chapter previous + 1: poliamory

283
99:59:59,999 --> 99:59:59,999
Emma Goldman said:

284
99:59:59,999 --> 99:59:59,999
If love does not know how to give and
take without restrictions,

285
99:59:59,999 --> 99:59:59,999
it is not love, but a transaction
that never fails to lay stress

286
99:59:59,999 --> 99:59:59,999
on a plus and a minus

287
99:59:59,999 --> 99:59:59,999
poliamory is according to wikipedia

288
99:59:59,999 --> 99:59:59,999
the practice, desire or acceptance
of intimate relationships

289
99:59:59,999 --> 99:59:59,999
that are not exclusive with respect to

290
99:59:59,999 --> 99:59:59,999
other sexual intimate relationships,
and, this is important,

291
99:59:59,999 --> 99:59:59,999
with knowledge and consent from
everyone involved

292
99:59:59,999 --> 99:59:59,999
so, a fling on the side dont ask, dont tell

293
99:59:59,999 --> 99:59:59,999
its generally not considered poliamory

294
99:59:59,999 --> 99:59:59,999
unless, theres knowledge and consent about
the dont ask dont tell part of it

295
99:59:59,999 --> 99:59:59,999
that is the concept

296
99:59:59,999 --> 99:59:59,999
the poliamory community has some
interesting things for us to learn

297
99:59:59,999 --> 99:59:59,999
one was pointed out in Vaumarcus
a couple of years ago

298
99:59:59,999 --> 99:59:59,999
nod if you want to be credited

299
99:59:59,999 --> 99:59:59,999
that wasnt a nod, i can take it as a ??
consense

300
99:59:59,999 --> 99:59:59,999
Rhonda pointed out something along
the line of

301
99:59:59,999 --> 99:59:59,999
if somebody takes good care of one
of your packages

302
99:59:59,999 --> 99:59:59,999
you should be happy, rather than angry

303
99:59:59,999 --> 99:59:59,999
the poliamorous community uses the word
compersion a lot

304
99:59:59,999 --> 99:59:59,999
which is exactly the feeling you get when
somebody else also takes good care

305
99:59:59,999 --> 99:59:59,999
of one of your packages

306
99:59:59,999 --> 99:59:59,999
talking about that

307
99:59:59,999 --> 99:59:59,999
we currently allow only one value
in the maintainer field in debian control

308
99:59:59,999 --> 99:59:59,999
which means that packages takes over
are traumatic experiences

309
99:59:59,999 --> 99:59:59,999
because the previous maintainer is dump
in favor of a new one

310
99:59:59,999 --> 99:59:59,999
and values can only be replaced

311
99:59:59,999 --> 99:59:59,999
if values could be added instead and
removed when they dont make sense anymore

312
99:59:59,999 --> 99:59:59,999
then the whole concept of somebody not
maintaining a packages ?? and so on

313
99:59:59,999 --> 99:59:59,999
could be solved by other people joining in
and relationships evolving, right?

314
99:59:59,999 --> 99:59:59,999
so thats a proposal for policy

315
99:59:59,999 --> 99:59:59,999
another interesting thing that comes out of
the poliamorous greater cloud of people

316
99:59:59,999 --> 99:59:59,999
is that different people have different
definitions of love

317
99:59:59,999 --> 99:59:59,999
different needs, different definitions of love
even

318
99:59:59,999 --> 99:59:59,999
so it happened to me to have a conversation
with a person and say

319
99:59:59,999 --> 99:59:59,999
so, how do you define love?
whats love for you?

320
99:59:59,999 --> 99:59:59,999
oh, great you asked me
that is fantastic because

321
99:59:59,999 --> 99:59:59,999
its not an standard thing

322
99:59:59,999 --> 99:59:59,999
if you ask a random person what is love
often the result is... its love, right?

323
99:59:59,999 --> 99:59:59,999
so, the definition ive adopted recently
and im still very happy about is this

324
99:59:59,999 --> 99:59:59,999
[I love you:]
[my world is better with you in it]

325
99:59:59,999 --> 99:59:59,999
with and idea like that, you can see
how it is possible

326
99:59:59,999 --> 99:59:59,999
to love several people at once

327
99:59:59,999 --> 99:59:59,999
it does not mean that one has
specific expectations

328
99:59:59,999 --> 99:59:59,999
towards the people one loves

329
99:59:59,999 --> 99:59:59,999
otherwise, life becomes difficult and
theres only 24h in a day

330
99:59:59,999 --> 99:59:59,999
there are people that makes my world better
but they dont smell in a way that i like

331
99:59:59,999 --> 99:59:59,999
and stuff like that

332
99:59:59,999 --> 99:59:59,999
[laughs]

333
99:59:59,999 --> 99:59:59,999
and the distinction between romantic love
and sexual love could be made

334
99:59:59,999 --> 99:59:59,999
you can see how the whole concept of relationship
gets deconstructed

335
99:59:59,999 --> 99:59:59,999
and one can have sexual, romantic
sexual and romantic

336
99:59:59,999 --> 99:59:59,999
no sexual, no romantic but plans for life

337
99:59:59,999 --> 99:59:59,999
??

338
99:59:59,999 --> 99:59:59,999
life gets really interesting

339
99:59:59,999 --> 99:59:59,999
and fortunately theres only 24h in a day
and i like ?? every night

340
99:59:59,999 --> 99:59:59,999
but we were talking about anarchism as well

341
99:59:59,999 --> 99:59:59,999
so i have done my ?? as well

342
99:59:59,999 --> 99:59:59,999
the readical idea than relationships can
be something that is negotiated

343
99:59:59,999 --> 99:59:59,999
between the people involved

344
99:59:59,999 --> 99:59:59,999
thats so radical

345
99:59:59,999 --> 99:59:59,999
why isnt like that all the time?

346
99:59:59,999 --> 99:59:59,999
to me is a big problem if a relationship
is something that is bound by rules

347
99:59:59,999 --> 99:59:59,999
that are not mutually agreed on
by the people involved, right?

348
99:59:59,999 --> 99:59:59,999
but, some call it, relationship anarchy

349
99:59:59,999 --> 99:59:59,999
so, going on, im 4 minutes behind

350
99:59:59,999 --> 99:59:59,999
Voltairine de Cleyre about
new debian contributors

351
99:59:59,999 --> 99:59:59,999
and trusting lintian warmings said:

352
99:59:59,999 --> 99:59:59,999
anarchism, to me, means not only the denial
of authority, not only a new economy,

353
99:59:59,999 --> 99:59:59,999
but a revision of the principles of morality

354
99:59:59,999 --> 99:59:59,999
It means the development of the individual
as well as the assertion of the individual

355
99:59:59,999 --> 99:59:59,999
It means self-responsible, and not leader
whorship

356
99:59:59,999 --> 99:59:59,999
so self-reponsability before blinding
trusting lintian warmings

357
99:59:59,999 --> 99:59:59,999
back to consent, i thought a lot about consent
because, having a person mildly say yes

358
99:59:59,999 --> 99:59:59,999
does not feel like right

359
99:59:59,999 --> 99:59:59,999
when things get complicated
you need to know what you are doing

360
99:59:59,999 --> 99:59:59,999
what situation you are putting yourself into

361
99:59:59,999 --> 99:59:59,999
which isnt often easy. Especially, if you
dont know the person

362
99:59:59,999 --> 99:59:59,999
you have in front of you. And if you dont
know the person you have in front of you

363
99:59:59,999 --> 99:59:59,999
you might not know what situation
you are putting yourself into

364
99:59:59,999 --> 99:59:59,999
and you deny consent until
you feel some trust

365
99:59:59,999 --> 99:59:59,999
you need to know that the person asking
a question really is able to acept

366
99:59:59,999 --> 99:59:59,999
any answer and take it seriously

367
99:59:59,999 --> 99:59:59,999
that is not necessarily true if you have
an application manager

368
99:59:59,999 --> 99:59:59,999
that asks a question to an applicant
some applicants go... [i dont know]

369
99:59:59,999 --> 99:59:59,999
what do you think of the social contract
and the applicant goes like...

370
99:59:59,999 --> 99:59:59,999
its perfect! i have no problem with it
but... can you trust that?

371
99:59:59,999 --> 99:59:59,999
and you need to feel that you have
alternatives, because if an applicant thinks

372
99:59:59,999 --> 99:59:59,999
if they say, i really do not understand why
in the DFSG we have an article about ??

373
99:59:59,999 --> 99:59:59,999
they might feel like if they say that
they will not become debian developers

374
99:59:59,999 --> 99:59:59,999
because something

375
99:59:59,999 --> 99:59:59,999
so if one doesnt feel theres
an alternative

376
99:59:59,999 --> 99:59:59,999
if things go wrong, there is ways out
and so on

377
99:59:59,999 --> 99:59:59,999
then consent is hard

378
99:59:59,999 --> 99:59:59,999
sometime ago, i came up with the idea of

379
99:59:59,999 --> 99:59:59,999
be selfish when you ask, honest when
you reply, and when others reply

380
99:59:59,999 --> 99:59:59,999
take them seriously, and if any of this
does not stand, i find it hard to trust

381
99:59:59,999 --> 99:59:59,999
that we are in a consensual situation

382
99:59:59,999 --> 99:59:59,999
and unfortunately i see little education
for consent

383
99:59:59,999 --> 99:59:59,999
in standard education, there is not much
consent

384
99:59:59,999 --> 99:59:59,999
dear child, this is
what you would like to do today

385
99:59:59,999 --> 99:59:59,999
this is what is important for you today

386
99:59:59,999 --> 99:59:59,999
otherwise, bad things happen

387
99:59:59,999 --> 99:59:59,999
or in the workplace, there is often
little consensuality

388
99:59:59,999 --> 99:59:59,999
the whole thing begins with, i need
to buy part of your consent with money

389
99:59:59,999 --> 99:59:59,999
some people are lucky to get paid
for what they really want to do

390
99:59:59,999 --> 99:59:59,999
some people not

391
99:59:59,999 --> 99:59:59,999
we will be followed by the cheese and wine
BOF, so there will be alcohol

392
99:59:59,999 --> 99:59:59,999
alcohol is not an excuse for breaking
consent

393
99:59:59,999 --> 99:59:59,999
and incidentally i heard reports of
borderline situations happening

394
99:59:59,999 --> 99:59:59,999
at debconf, if something makes you
uncomfortable, you can talk to people

395
99:59:59,999 --> 99:59:59,999
at frontdesk

396
99:59:59,999 --> 99:59:59,999
if people in frontdesk makes you
uncomfortable, you can talk to me

397
99:59:59,999 --> 99:59:59,999
if both me and frontdesk makes you
uncomfortable, call the police

398
99:59:59,999 --> 99:59:59,999
audience: [inaudible]
good point

399
99:59:59,999 --> 99:59:59,999
thats a very good point

400
99:59:59,999 --> 99:59:59,999
can i have 5 more minutes?

401
99:59:59,999 --> 99:59:59,999
practical advice

402
99:59:59,999 --> 99:59:59,999
suppose you want relationship advice
or work advice

403
99:59:59,999 --> 99:59:59,999
they are pretty much the same,
like this is a nice article about

404
99:59:59,999 --> 99:59:59,999
how to deal with abusive relationship
or bad work places

405
99:59:59,999 --> 99:59:59,999
keep them tired,
keep them too busy to think

406
99:59:59,999 --> 99:59:59,999
keep them emotionally involved
reward intermittently

407
99:59:59,999 --> 99:59:59,999
keep the crises rolling,
things will be better when...

408
99:59:59,999 --> 99:59:59,999
we figured some time ago that if you go on
a situation thinking, it will be better

409
99:59:59,999 --> 99:59:59,999
it never will

410
99:59:59,999 --> 99:59:59,999
keep real rewards distant, and so on

411
99:59:59,999 --> 99:59:59,999
more practical advice
i can give links later

412
99:59:59,999 --> 99:59:59,999
this is an excellent article

413
99:59:59,999 --> 99:59:59,999
[what technical recruiters
can learn from online dating]

414
99:59:59,999 --> 99:59:59,999
i highly recommend it, because then
you can really see the recruiter email

415
99:59:59,999 --> 99:59:59,999
very similar to some shady person
somewhere coming from behind

416
99:59:59,999 --> 99:59:59,999
and saying: do you come here often?

417
99:59:59,999 --> 99:59:59,999
more relationship advice from
99 ways to ruin an open source project

418
99:59:59,999 --> 99:59:59,999
how to ruin the trust of the community

419
99:59:59,999 --> 99:59:59,999
exert excessive control

420
99:59:59,999 --> 99:59:59,999
never give anyone else commit access

421
99:59:59,999 --> 99:59:59,999
i dont know what that was,
require an unparseable unicode character

422
99:59:59,999 --> 99:59:59,999
ignore concerns voiced in issues

423
99:59:59,999 --> 99:59:59,999
do any of this in a relationship and
it wont last long, right?

424
99:59:59,999 --> 99:59:59,999
and so on

425
99:59:59,999 --> 99:59:59,999
i highly also recommend this thing
making relationships suck

426
99:59:59,999 --> 99:59:59,999
its a wonderful read, i can provide links
somehow later

427
99:59:59,999 --> 99:59:59,999
i dont know where to publish them
but we can work it out

428
99:59:59,999 --> 99:59:59,999
theres also this

429
99:59:59,999 --> 99:59:59,999
BSDM advice, tips for new dominants

430
99:59:59,999 --> 99:59:59,999
so, tips for new packages maintainers

431
99:59:59,999 --> 99:59:59,999
you will fuck up

432
99:59:59,999 --> 99:59:59,999
and so on

433
99:59:59,999 --> 99:59:59,999
i guess is time to close, i hope
i gave an idea on how to carry across

434
99:59:59,999 --> 99:59:59,999
advise thought quite a lot over the years
between one domain to another

435
99:59:59,999 --> 99:59:59,999
when theres an interesting point
that will overlap

436
99:59:59,999 --> 99:59:59,999
and id like to close recalling
my definition of love

437
99:59:59,999 --> 99:59:59,999
dear debian and dear everyone contributing
to debian

438
99:59:59,999 --> 99:59:59,999
my world is better with you in it
&lt;3

439
99:59:59,999 --> 99:59:59,999
thank you, thank you, thank you

440
99:59:59,999 --> 99:59:59,999
talkmaister: any questions?
